#include "definitions.h"
#include "misc.h"

#include <cuda_runtime.h>

int getDeviceCount()
{
    int deviceCount;
    CUDA_CHECK_STATUS(cudaGetDeviceCount(&deviceCount));
    return deviceCount;
}
