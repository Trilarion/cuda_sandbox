#include "sandbox.h"
#include "misc.h"

#include <string>

std::string last_error;

int cuda_available()
{
    // Returns 1 if CUDA is available and 0 otherwise
    try
    {
        getDeviceCount();
        return 1;
    }
    catch (std::exception & exception)
    {
        last_error = exception.what();

        return 0;
    }
}
