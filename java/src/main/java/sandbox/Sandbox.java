package sandbox;

public class Sandbox {

    static {
        System.loadLibrary("SandboxJNI");
    }

    public static native int add(int a, int b);

    public static native int cuda_available();
}
