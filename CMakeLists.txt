cmake_minimum_required( VERSION 3.8 )

project( cuda_sandbox CXX CUDA )

# library

set( CXX_HEADERS
	sandbox.h
	definitions.h
	misc.h
)

set( CXX_SOURCES
	sandbox.cpp
	misc.cpp
)

set( CUDA_HEADERS
	sandbox.cuh
)

set( CUDA_SOURCES
	sandbox.cu
)

source_group("CUDA Header Files" FILES ${CUDA_HEADERS})
source_group("CUDA Source Files" FILES ${CUDA_SOURCES})

add_library( Sandbox SHARED
	${CXX_HEADERS}
	${CXX_SOURCES}
	${CUDA_HEADERS}
	${CUDA_SOURCES}
)

set_target_properties( Sandbox
	PROPERTIES
		CXX_VISIBILITY_PRESET hidden
		DEFINE_SYMBOL _COMPILING_SANDBOX
)

# Java adapter

find_package( JNI )

if( NOT JNI_FOUND )
  message( ERROR "JAVA JNI NOT found" )
  return()
endif()

set( JAVA_ADAPTER_HEADERS
	jsandbox.h
)

set( JAVA_ADAPTER_SOURCES
	jsandbox.cpp
)

add_library( SandboxJNI SHARED
 	${JAVA_ADAPTER_HEADERS}
 	${JAVA_ADAPTER_SOURCES}
)

target_include_directories( SandboxJNI PRIVATE ${PROJECT_SOURCE_DIR} ${JNI_INCLUDE_DIRS} )
target_link_libraries( SandboxJNI Sandbox ${JNI_LIBRARIES} )