#include "jsandbox.h"
#include "sandbox.h"

/*
* Class:     sandbox_Sandbox
* Method:    add
* Signature: (II)I
*/
JNIEXPORT jint JNICALL Java_sandbox_Sandbox_add(JNIEnv * env, jclass cls, jint a, jint b)
{
    return a + b;
}

/*
* Class:     sandbox_Sandbox
* Method:    cuda_available
* Signature: ()I
*/
JNIEXPORT jint JNICALL Java_sandbox_Sandbox_cuda_1available(JNIEnv * env, jclass cls)
{
    return cuda_available();
}