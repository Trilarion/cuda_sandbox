#ifndef SANDBOX_H_INCLUDED
#define SANDBOX_H_INCLUDED

#ifdef __linux__
#	define VISIBLE __attribute__((visibility("default")))
#endif
#ifdef _WIN32
#	ifndef _COMPILING_SANDBOX
#		define VISIBLE __declspec(dllimport)
#	else
#		define VISIBLE __declspec(dllexport)
#	endif 
#endif

#ifdef __cplusplus
extern "C" {
#endif

    // returns 1 if cuda is available and 0 otherwise
    VISIBLE int cuda_available();

#ifdef __cplusplus
}
#endif

#endif // SANDBOX_H_INCLUDED
